#include <pthread.h>
#include <vector>

#define __DEQUE_DATA_SIZE (1 << 23)

template<typename T>
class deque {
  unsigned long int size;
  std::vector<T> data;
  
  unsigned long int front;
  unsigned long int back;

  pthread_cond_t full_cond;
  pthread_cond_t empty_cond;

  pthread_mutex_t lock;
  
 public:
  deque(unsigned long int size = __DEQUE_DATA_SIZE) {
    this->front = 0;
    this->back = 0;
    this->size = size;
    this->data = std::vector<T>(size);

    pthread_mutex_init(&lock, NULL);
    pthread_cond_init(&full_cond, NULL);
    pthread_cond_init(&empty_cond, NULL);
  }

  virtual long int num_elems() {
    return back - front;
  }

  virtual long int get_size () {
    return size;
  }
  
  T pop_front() {
    pthread_mutex_lock(&lock);
    while(front == back)
      pthread_cond_wait(&empty_cond, &lock);
    front--;
    T elem = data[front % size];
    pthread_cond_broadcast(&full_cond);
    pthread_mutex_unlock(&lock);
    return elem;
  }
  
  T pop_back() {
    pthread_mutex_lock(&lock);
    while(front == back)
      pthread_cond_wait(&empty_cond, &lock);
    T elem = data[back % size];
    back++;
    pthread_cond_broadcast(&full_cond);
    pthread_mutex_unlock(&lock);
    return elem;
  }
  
  void push_front(T elem) {
    pthread_mutex_lock(&lock);
    while(back - front == size)
      pthread_cond_wait(&full_cond, &lock);
    data[front % size] = elem;
    front++;
    pthread_cond_broadcast(&empty_cond);
    pthread_mutex_unlock(&lock);
  }
  
  void push_back(T elem) {
    pthread_mutex_lock(&lock);
    while(front - back == size)
      pthread_cond_wait(&full_cond, &lock);
    back--;
    data[back % size] = elem;
    pthread_cond_broadcast(&empty_cond);
    pthread_mutex_unlock(&lock);
  }
  
  
  virtual ~deque() {
    std::cout << "called destructor" << std::endl;
  }
};
